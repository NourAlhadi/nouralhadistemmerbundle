<?php
namespace Nouralhadi\StemmerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class StemmerController extends Controller{
    
    /**
     * @Route("/stemmer",name="stemmer")
     */
    public function indexAction(){
        return $this->render('index.html.twig');
    }

}