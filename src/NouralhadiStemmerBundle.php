<?php

namespace Nouralhadi\StemmerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Vendor\NanoFrameworkBundle\DependencyInjection\CompilerPass\AddListenersPass;

class NouralhadiStemmerBundle extends Bundle{

	public function build(ContainerBuilder $container){
        parent::build($container);

        $container->addCompilerPass(new AddListenersPass());
    }

}
